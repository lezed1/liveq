Questions = new Meteor.Collection "questions"
Answers = new Meteor.Collection "answers"
Usernames = new Meteor.Collection "usernames"

Template.layout.error = ->
  Session.get "error"

# Always keep user information up to date
Deps.autorun ->
  Meteor.subscribe "user", Meteor.user()?._id

#Returns the current user's name, or false
username = ->
  Usernames.findOne(Meteor.user()?._id)?.username
  
Template.name.username = username
Template.layout.namePrompt = ->
  not Meteor.user()? or username()

# Returns true if the current user owns the question
Template.question.isOwner = ->
  @owner == Meteor.user()?._id
  
# Returns true if the owner of the answer should be displayed
Template.question.displayOwner = ->
  user = Meteor.user()
  unless user?
    return
  
  Questions.findOne(@question).owner is user._id

# Returns all answers to a quesiton
Template.question.answers = ->
  Answers.find
    question: @_id

# Returns the owner of the question
Template.question.getOwner = ->
  Usernames.findOne(@owner)?.username or "Mr. No-Name. (#{@owner})"
  
# Return the content of the user tooltip
Template.question.tooltipContent = ->
  "Email: #{Usernames.findOne(@owner)?.email}"

# Set editing flag to session value
Template.question.editing = ->
  Session.get "editing"

# Events
$ ->
  #DIRTY HACK: could find the right event, so....
  setInterval (-> $('.tooltips').tooltip()), 1000
  
  # Set handlers
  handle = (input, button, func) ->
    if input?
      $('body').on "keypress", input, (e) ->
        if e.keyCode == 13 and $(input).val()
          func()
    if button?
      $('body').on "click", button, ->
        if $(input).val()
          func()
  
  # Create a new question
  newQ = ->
    Meteor.call "newQ", $("#newQText").val(),  $("#newQID").val(), (err, result) ->
      console.log err, result
      if err?
        return Session.set "error", err.reason
      Router.go "/q/" + result
      
  handle "#newQText", "#createQ", newQ
  handle "#newQID", null, newQ  
  
  # Submit answer to the server
  handle "#answer", "#answerBtn", ->
    Meteor.call "submitAnswer", Router.current().params.id, $("#answer").val(), (err, result) ->
      if err
        return Session.set "error", err.reason
    $("#answer").val("")
  
  # Submit name change to the server
  handle "#name", "#updateName", ->
    if $("#name")?.val()
      Meteor.call "updateName", $("#name").val(), (err, result) ->
        if err
          console.log err
          return Session.set "error", err.reason
        Router.go "/"
  
  # Toggle question edit mode
  $('body').on "click", "#edit", (e) ->
    Session.set "editing", !Session.get "editing"
    
  # Remove answers to current question
  $('body').on "click", "#delete", (e) ->
    if confirm "Do you want to delete all answers to this question?"
      Meteor.call "deleteQuestionAnswers", Router.current().params.id
  
  # Update question text
  handle "#QText", "#updateQuestion", ->
    if $("#QText").val()
      Meteor.call "updateQuestionText", Router.current().params.id, $("#QText").val()
      Session.set "editing", false
      
  $('body').on "click", "#closeError", (e) ->
    Session.set "error", false  

# Stubs
submitAnswer = (q, answer) ->
  user = Meteor.user()
  if not user?
    throw new Meteor.Error 401, "Must be signed in to answer a quesion"
  Answers.insert
    owner: user
    text: answer
    question: q

Meteor.methods
  submitAnswer: submitAnswer

# Routes
Router.configure
  layout: "layout"

Router.map ->
  @route "home",
    path: "/"
  @route "new"
  @route "name"
  @route "question",
    path: "/q/:id"
    data: ->
      Questions.findOne @params.id
    onBeforeRun: ->
      Meteor.subscribe "questionInfo", @params.id