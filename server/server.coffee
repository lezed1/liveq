Questions = new Meteor.Collection "questions"
Answers = new Meteor.Collection "answers"
Usernames = new Meteor.Collection "usernames"

Meteor.startup ->
  # Send verification email on user creation
  Accounts.config
    sendVerificationEmail: true
    forbidClientAccountCreation: false
  
  # Set options for validation emails
  Accounts.emailTemplates.from = "LiveQ Accounts <no-reply@live1.lezed1.tk>"
  Accounts.emailTemplates.siteName = "LiveQ"  
  
  
  Meteor.publish "user", (user) ->
    Usernames.find
      _id: user
  
  Meteor.publish "users", (users) ->
    Usernames.find
      _id:
        $in: users
  
  Meteor.publish "questionInfo", (question) ->
    if @userId == Questions.findOne(question).owner
      result = [Questions.find(question),
        Answers.find
           question: question
        ]
      
      users = []
      result[1].forEach (answer) ->
        users.push answer.owner
      
      result[2] = Usernames.find
        _id:
          $in: _.unique(users)
      
      return result
    
    else
      [Questions.find(question),
       Answers.find
         question: question
         owner: @userId
      ]
      
verified = (user) ->
  user?.emails[0].verified

newQ = (text, id) ->
  user = Meteor.user()
  unless user?
    throw new Meteor.Error 401, "Must be signed in to ask a question"
  unless verified user
    throw new Meteor.Error 401, "Email must be verified to ask a question"
  if id
    Questions.insert
      _id: id
      owner: user._id
      text: text
      group: []
  else
    Questions.insert
      owner: user._id
      text: text
      group: []

submitAnswer = (q, answer) ->
  user = Meteor.user()
  unless user?
    throw new Meteor.Error 401, "Must be signed in to answer a quesion"
  unless Usernames.findOne(Meteor.user()?._id)?.username
    throw new Meteor.Error 401, "Must have name set to answer a question"
  Answers.insert
    owner: Meteor.user()._id
    text: answer
    question: q

updateName = (name) ->
  user = Meteor.user()?._id
  unless Usernames.findOne user
    Usernames.insert
      _id: user
      username: name
      email: Meteor.users.findOne(user)?.emails[0].address
  else
    Usernames.update user,
      $set:
        username: name

updateQuestionText = (q, text) ->
  Questions.update 
    _id: q
    owner: Meteor.user()._id
  ,
    $set:
      text: text
      
deleteQuestionAnswers = (q) ->
  if Meteor.user()._id == Questions.findOne(q).owner
    Answers.remove
      question: q
    
Meteor.methods
  newQ: newQ
  submitAnswer: submitAnswer
  updateName: updateName
  updateQuestionText: updateQuestionText
  deleteQuestionAnswers: deleteQuestionAnswers